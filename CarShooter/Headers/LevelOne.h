#ifndef LEVEL_ONE_H
#define LEVEL_ONE_H

#include "EnemyVehicle.h"
#include "Explotion.h"
#include "HUD.h"
#include "Obstacle.h"
#include "PlayerVehicle.h"
#include "TextureHandler.h"

namespace CoreGame {

	class LevelOne {

		sf::RenderWindow& mWindow;
		TextureHandler mTextures;
		ClassVehicle::PlayerVehicle* mPlayerVehicle;
		sf::Vector2f* mCheckpoints;
		sf::View mView;

		std::vector<Collider::Obstacle> mObstaclesList;
		std::vector<ClassVehicle::EnemyVehicle *> mEnemiesList;
		sf::Sprite mBackground;

		// TO TEST, LATER HAVE TO BE A VECTOR OF EXPLOTIONS
		std::vector<ClassVehicle::Explotion *> mExplotions;

		void setObstacles();
		void setEnemies();

	public:

		// Constructor
		LevelOne(sf::RenderWindow& aWindow);
		
		// Destructor
		~LevelOne();

		void obstacleColide(ClassVehicle::Vehicle& aCar);

		inline unsigned int getEnemiesNumber() const { return (unsigned int)mEnemiesList.size(); }

		inline sf::Sprite getBackground() { return mBackground; }
		inline ClassVehicle::Vehicle& getPlayerCar() const { return *mPlayerVehicle; }
		
		// The following needs to be in another class
		void generalMovement(float& deltaTime);
		void shootBullet(float& deltaTime);
		void handleBullets(ClassVehicle::Vehicle& aCar);

		// The Following needs to be in another class
		sf::Vector2f getViewCoordinates() const { return mView.getCenter(); }

	};

}

#endif