#ifndef CORE_GAME_H
#define CORE_GAME_H

#include "LevelOne.h"
#include "MainMenu.h"
#include "WonScreen.h"

namespace CoreGame {

	class CoreGame {

		enum State {
			S_MENU,
			S_LVL_SELECT,
			S_PLAYING,
			S_LOSE
		};

		sf::RenderWindow mWindow;
		State mCurrentState;
		HUD* mLevelHUD;
		LevelOne* mLevelOne;
		unsigned int mEnemiesAlive;
		GeneralMenu* mMenu;

		
	public:
		CoreGame();
		~CoreGame();

		void handleMenu(const sf::Event aEvent);
		void playerWon();

		void runGame();

	};


}

#endif