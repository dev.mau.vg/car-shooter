#ifndef ENEMY_VEHICLE_H
#define ENEMY_VEHICLE_H

#include "Vehicle.h"

namespace ClassVehicle {

	class EnemyVehicle : public Vehicle {
		
		// Set a state to change between the following:
		// --Self Drive: The vehicle moves from point to point in the map
		//		until it founds a specific car (it's location)
		// --Following: The vehicle follows a specific car until it's too far
		//		to keep track
		// --Lost: The behicle have to find the nearest point to get back
		//		to the parh of the map

		// Self Drive -> Following -> Lost -> Self Drive

		// Enjoy

		enum vehicleState {
			SELF_DRIVE,
			FOLLOWING
		};

		int mActualIndex;
		sf::Vector2f* mCheckpointsVector;
		int mNumberOfCheckpoints;
		vehicleState mCurrentState;

	public:
		EnemyVehicle(float modelWidth, float modelHeight,
			sf::Texture& vehicleTexture, sf::Texture& explotionTexture,
			float aMaxSpeed, float aSpawnX, float aSpawnY,
			sf::Vector2f* aCheckpointsVector, int aNumberOfCheckpoints);
		~EnemyVehicle();

		void inteligentMovement(sf::Vector2f& mainPlayerPosition);

		void searchNearestCheckpoint();



		sf::Vector2f checkState(sf::Vector2f& mainPlayerPosition);
	};

}

#endif