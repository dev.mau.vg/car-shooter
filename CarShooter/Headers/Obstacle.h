#ifndef OBSTACLE_H
#define OBSTACLE_H

#include <SFML\Graphics.hpp>

#include "Collider.h"

namespace Collider {

	class Obstacle {

		sf::RectangleShape mBody;

	public:

		// Constructor
		Obstacle(sf::Vector2f aSize, sf::Vector2f aPosition);

		// Copy Constructor
		Obstacle(const Obstacle& aObstacle);

		// Destructor
		~Obstacle();

		inline void draw(sf::RenderWindow& window) { window.draw(mBody); }

		inline Collider getCollider() { return Collider(mBody); }

	};

}

#endif