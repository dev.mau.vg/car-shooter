#ifndef EXPLOTION_H
#define EXPLOTION_H

#include "SFML\Graphics.hpp"
#include "Animation.h"

namespace ClassVehicle {

	class Explotion {

		sf::RectangleShape mModel;
		sf::Texture* mModelTexture;
		CoreGame::Animation mAnimation;
		unsigned int mRow;
		bool mFinished;
		float mTotalTime;
		float mSwitchTime;

	public:


		// Contructor
		Explotion(sf::Texture* aTexture, sf::Vector2u aImageCount,
			float aSwitchtime, sf::Vector2f aPosition);

		// Copy Constructor
		Explotion(const Explotion& aExplotion);

		// Destructor
		~Explotion();

		void update(float deltaTime);
		void draw(sf::RenderWindow& aWindow);
		bool isFinished() const { return mFinished; }
	};

}
#endif