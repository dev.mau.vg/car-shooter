#ifndef PLAYER_VEHICLE_H
#define PLAYER_VEHICLE_H

#include "Vehicle.h"

namespace ClassVehicle {

	class PlayerVehicle : public Vehicle {
		
	public:

		// Constructor
		PlayerVehicle(float modelWidth, float modelHeight,
			sf::Texture* vehicleTexture,
			float aMaxSpeed, float aSpawnX, float aSpawnY);

		// Copy Constructor
		PlayerVehicle(const PlayerVehicle& aPlayerVehicle);

		~PlayerVehicle();

		void shootBullet(float deltaTime) override;

		void animationMovement(float deltaTime);
	};

}

#endif