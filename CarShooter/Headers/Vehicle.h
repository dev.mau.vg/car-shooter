#ifndef VEHICLE_H
#define VEHICLE_H

#include "Animation.h"
#include "Collider.h"
#include "Bullet.h"

#include <stdlib.h>
#include <string>
#include <SFML/Graphics.hpp>

namespace Collider { class Collider; }

namespace ClassVehicle {

	class Vehicle {

	protected:

		sf::RectangleShape mModel;
		sf::Texture* mModelTexture;
		std::vector<Weapon::Bullet> mBulletList;
		int mLife;
		float mSpeed;
		float mMaxSpeed;
		sf::Vector2f mSpawn;
		float mAngle;
		float mTotalTime;

	public:

		// CONSTRUCTOR
		Vehicle(float modelWidth, float modelHeight,
				sf::Texture* vehicleTexture,
				float aMaxSpeed, float aSpawnX, float aSpawnY);

		// COPY CONSTRUCTOR
		Vehicle(const Vehicle& aVehicle);

		// DESTRUCTOR
		~Vehicle();

		// GETTERS
		inline sf::RectangleShape getModel() const { return mModel; }
		inline sf::Vector2f getPosition() const { return mModel.getPosition(); }
		inline float getSpeed() const { return mSpeed; }
		inline float getRotation() const { return mModel.getRotation(); }
		Collider::Collider getCollider() { return (Collider::Collider(mModel)); }
		std::vector<Weapon::Bullet>& getBullets() { return mBulletList; }
		
		// SETTERS
		inline void setSpeed(const float aSpeed) { mSpeed = aSpeed; }
		inline void setBulletDemage() { mLife -= 10; }
		inline void rotateVehicle(float angle) { mModel.rotate(angle); }

		inline void moveVehicle(const float x, const float y) {
			mModel.move(x, y);
		}
		
		virtual void shootBullet(float deltaTime);
		void drawBullet(sf::RenderWindow& aWindow);


		inline bool isDead() { return (mLife <= 0); }
		

	};

}

#endif