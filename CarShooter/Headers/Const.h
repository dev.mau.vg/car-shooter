#ifndef CONST_H
#define CONST_H

#define TURN_RIGHT		2.5f
#define TURN_LEFT		-2.5f
#define ACC				0.5f
#define BULLET_SPEED	50
#define BULLET_DELAY	0.1f

// Menu Things
#define MAIN_MENU_ITEMS 2
#define WON_MENU_ITEMS	2
#define MAX_MENU_ITEMS	5

// Textures 
#define YELLOW_CAR		"Textures/yellowCar.png"
#define RED_CAR			"Textures/redCar.png"
#define PINK_CAR		"Textures/pinkCar.png"
#define WHITE_CAR		"Textures/whiteCar.png"
#define EXPLOTION		"Textures/carExplotion.png"
#define BG_LEVEL_ONE	"Textures/levelOne.png"

#endif
