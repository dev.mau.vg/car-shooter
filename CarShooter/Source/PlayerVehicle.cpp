#include "../Headers/PlayerVehicle.h"
#include "../Headers/Const.h"

#include <iostream>

namespace ClassVehicle {

	PlayerVehicle::PlayerVehicle(float modelWidth, float modelHeight,
		sf::Texture* vehicleTexture,
		float aMaxSpeed, float aSpawnX, float aSpawnY)
		: Vehicle(modelWidth, modelHeight, vehicleTexture,
			aMaxSpeed, aSpawnX, aSpawnY)
	{
	}


	PlayerVehicle::PlayerVehicle(const PlayerVehicle& aPlayerVehicle)
		: Vehicle(aPlayerVehicle.mModel.getSize().x,
				  aPlayerVehicle.mModel.getSize().y,
				  aPlayerVehicle.mModelTexture,
				  aPlayerVehicle.mMaxSpeed,
				  aPlayerVehicle.mSpawn.x,
				  aPlayerVehicle.mSpawn.y)
	{
	}

	PlayerVehicle::~PlayerVehicle()	{
	}

	void PlayerVehicle::shootBullet(float deltaTime) {
		mTotalTime += deltaTime;
		if (mTotalTime >= BULLET_DELAY) {
			mTotalTime -= BULLET_DELAY;
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
				mBulletList.push_back(
					Weapon::Bullet(mModel.getPosition(), mAngle, mSpeed)
				);
			}
		}
	}

	void PlayerVehicle::animationMovement(float deltaTime) {

		mAngle = (3.1415926536f / 180)*(mModel.getRotation());

		float forx = 0.2f*-sin(mAngle);
		float fory = 0.2f*cos(mAngle);

		//mSpeed *= deltaTime;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
			if (mSpeed >= 0) {
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
					rotateVehicle(TURN_RIGHT);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
					rotateVehicle(TURN_LEFT);
				}
			}
			else {
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
					rotateVehicle(TURN_LEFT);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
					rotateVehicle(TURN_RIGHT);
				}
			}

			if (mSpeed < mMaxSpeed) mSpeed += ACC;
			
			moveVehicle(-forx*mSpeed, -fory*mSpeed);
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {

			if (mSpeed > -mMaxSpeed) mSpeed -= ACC;

			if (mSpeed < 0) {

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
					rotateVehicle(TURN_LEFT);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
					rotateVehicle(TURN_RIGHT);
				}

				moveVehicle(forx*abs(mSpeed), fory*abs(mSpeed));
			}
			else {

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
					rotateVehicle(TURN_RIGHT);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
					rotateVehicle(TURN_LEFT);
				}

				moveVehicle(-(forx*mSpeed), -(fory*mSpeed));
			}
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::S) &&
			!sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
			if (mSpeed - ACC > 0) mSpeed -= ACC;
			else if (mSpeed + ACC < 0) mSpeed += ACC;
			else mSpeed = 0;

			if (mSpeed > 0) {
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
					rotateVehicle(TURN_RIGHT);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
					rotateVehicle(TURN_LEFT);
				}

				moveVehicle(-forx*abs(mSpeed), -fory*abs(mSpeed));

			}
			else if (mSpeed < 0) {
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
					rotateVehicle(TURN_LEFT);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
					rotateVehicle(TURN_RIGHT);
				}

				moveVehicle(forx*abs(mSpeed), fory*abs(mSpeed));

			}

		}

	}

}