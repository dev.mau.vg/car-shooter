#include "../Headers/GeneralMenu.h"
#include "../Headers/Const.h"

namespace CoreGame {

	GeneralMenu::GeneralMenu(sf::RenderWindow& aWindow)
		: mElementSelected(0)
		, mWindow(aWindow)
		, mElements(0)
		, mView(sf::View(sf::Vector2f(0.0f, 0.0f),
				sf::Vector2f(800.0f, 600.0f)))
	{
		mView.setCenter(400.0f, 300.0f);
		mWindow.setView(mView);

		if (!mFont.loadFromFile("Textures/arial.ttf")) {
			// Handle Error
		}

		for (unsigned int i = 0; i < MAX_MENU_ITEMS; ++i) {
			mMenu.push_back(sf::Text());
			mMenu[i].setFont(mFont);
			mMenu[i].setFillColor(sf::Color::White);
			mMenu[i].setPosition(sf::Vector2f(mView.getSize().x / 1.5f,
				((i + 1) * 50.0f)));
		}
	}

	GeneralMenu::GeneralMenu(const GeneralMenu& aGeneralMenu)
		: mMenu				(aGeneralMenu.mMenu)
		, mFont				(aGeneralMenu.mFont)
		, mWindow			(aGeneralMenu.mWindow)
		, mElementSelected	(aGeneralMenu.mElementSelected)
		, mElements			(aGeneralMenu.mElements)
		, mView				(aGeneralMenu.mView)
	{

	}

	GeneralMenu::~GeneralMenu()	{
	}


	void GeneralMenu::draw() {

		// TODO MAKE IT VIRTUAL AND OVERRIDE IN WON SCREEN

		for (unsigned int i = 0; i < mElements; ++i) {
			mWindow.draw(mMenu[i]);
			std::string coso = mMenu[i].getString();
		}
	}

	void GeneralMenu::moveUp() {

		if (mElementSelected - 1 >= 0) {
			mMenu[mElementSelected].setFillColor(sf::Color::White);
			mElementSelected--;
			mMenu[mElementSelected].setFillColor(sf::Color::Red);
		}
	}

	void GeneralMenu::moveDown() {
		if (mElementSelected + 1 < mElements) {
			mMenu[mElementSelected].setFillColor(sf::Color::White);
			mElementSelected++;
			mMenu[mElementSelected].setFillColor(sf::Color::Red);
		}
	}

}