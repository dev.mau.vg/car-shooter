#include "../Headers/Collider.h"

#include <iostream>

namespace Collider {


	Collider::Collider(sf::RectangleShape& aBody) : mBody(aBody) {
	}


	Collider::~Collider()
	{
	}

	bool Collider::ckeckCollision(Collider aOther, float push)
	{

		sf::Vector2f otherPosition = aOther.getPosition();
		sf::Vector2f otherHalfSize = aOther.getHalfSize();

		sf::Vector2f selfPosition = getPosition();
		sf::Vector2f selfHalfSize = getHalfSize();

		float deltaX = otherPosition.x - selfPosition.x;
		float deltaY = otherPosition.y - selfPosition.y;

		float intersectX = abs(deltaX) - (otherHalfSize.x + selfHalfSize.x);
		float intersectY = abs(deltaY) - (otherHalfSize.y + selfHalfSize.y);

		if (intersectX < 0.0f && intersectY < 0.0f) {

			push = std::min(std::max(push, 0.0f), 1.0f);

			if (intersectX > intersectY) {

				if (deltaX > 0.0f) {
					move(intersectX * (1.0f - push), 0.0f);
					aOther.move(-intersectX * push, 0.0f);
				} else {
					move(-intersectX * (1.0f - push), 0.0f);
					aOther.move(intersectX * push, 0.0f);
				}

			} else {
				if (deltaY > 0.0f) {
					move(0.0f, intersectY * (1.0f - push));
					aOther.move(0.0f, -intersectY * push);
				} else {
					move(0.0f, -intersectY * (1.0f - push));
					aOther.move(0.0f, intersectY * push);
				}
			}

			return true;
		}

		return false;
	}

}