#include <cmath>

#include "../Headers/Const.h"
#include "../Headers/EnemyVehicle.h"

#include <iostream>

namespace ClassVehicle {

	EnemyVehicle::EnemyVehicle(float modelWidth, float modelHeight,
		sf::Texture* vehicleTexture,
		float aMaxSpeed, float aSpawnX, float aSpawnY,
		sf::Vector2f* aCheckpointsVector, int aNumberOfCheckpoints)
		: Vehicle(modelWidth, modelHeight, vehicleTexture,
			aMaxSpeed, aSpawnX, aSpawnY)
		, mCheckpointsVector(aCheckpointsVector)
		, mNumberOfCheckpoints(aNumberOfCheckpoints)
		, mActualIndex(-1)
		, mCurrentState(SELF_DRIVE)
	{
		searchNearestCheckpoint();
	}


	EnemyVehicle::~EnemyVehicle() {
	}

	void  EnemyVehicle::searchNearestCheckpoint() {

		// Actual position
		sf::Vector2f actualPosition = mModel.getPosition();

		// For the lengh of the smaller Vector
		float smallerNorm = sqrt(std::pow(actualPosition.x - mCheckpointsVector[0].x, 2)
			+ std::pow(actualPosition.y - mCheckpointsVector[0].y, 2));

		// To check against the calculation
		float testingNorm;

		for (int i = 0; i < mNumberOfCheckpoints; ++i) {

			// Searching the nearest checkpoint

			testingNorm = sqrt(std::pow(mCheckpointsVector[i].x - actualPosition.x, 2)
				+ std::pow(mCheckpointsVector[i].y - actualPosition.y, 2));

			if ((testingNorm < smallerNorm) && mActualIndex != i) {
				smallerNorm = testingNorm;
				mActualIndex = i;
			}
		}
	}

	sf::Vector2f EnemyVehicle::checkState(sf::Vector2f& mainPlayerPosition) {
		
		sf::Vector2f nearestPosition;

		// Cheking the distance to the player car
		nearestPosition.y = (mainPlayerPosition.y - mModel.getPosition().y);
		nearestPosition.x = (mainPlayerPosition.x - mModel.getPosition().x);

		switch (mCurrentState) {
		case SELF_DRIVE:
			if (abs(nearestPosition.y) < 120 &&
				abs(nearestPosition.x) < 120) {
				mCurrentState = FOLLOWING;
				return checkState(mainPlayerPosition);
			} else {
				nearestPosition.y = (mCheckpointsVector[mActualIndex].y - mModel.getPosition().y);
				nearestPosition.x = (mCheckpointsVector[mActualIndex].x - mModel.getPosition().x);

				if (abs(nearestPosition.y) < 110 && abs(nearestPosition.x) < 110) {
					if (mActualIndex == mNumberOfCheckpoints - 1) {
						mActualIndex = 0;
					}
					else {
						mActualIndex++;
					}
				}

			}
			break;
		case FOLLOWING:

			if (abs(nearestPosition.y) > 170 &&
				abs(nearestPosition.x) > 170) {
				mCurrentState = SELF_DRIVE;
				searchNearestCheckpoint();
				return checkState(mainPlayerPosition);
			}

			break;
		}

		return nearestPosition;
	}



	void EnemyVehicle::inteligentMovement(sf::Vector2f& mainPlayerPosition) {

		// Destiny position
		sf::Vector2f nearestPosition = checkState(mainPlayerPosition);

		// Checking the quadrants

		// First Quadrant
		float rotation = ((int)mAngle * 180) / 3.1415926536f;

			if (nearestPosition.x > 0 && nearestPosition.y < 0) {
				if (rotation < 45.0f || rotation > 225.0f) {
					rotateVehicle(TURN_RIGHT);
				}
				else {
					rotateVehicle(TURN_LEFT);
				}
			}

			// Second Quadrant
			else if (nearestPosition.x < 0 && nearestPosition.y < 0) {
				if (rotation < 135 || rotation > 315) {
					rotateVehicle(TURN_LEFT);
				}
				else {
					rotateVehicle(TURN_RIGHT);
				}
			}

			// Third Quadrant
			else if (nearestPosition.x > 0 && nearestPosition.y > 0) {
				if (rotation < 135 || rotation > 315) {
					rotateVehicle(TURN_RIGHT);
				}
				else {
					rotateVehicle(TURN_LEFT);
				}
			}

			// Fourth Quadrant
			else {
				if (rotation < 45 || rotation > 225) {
					rotateVehicle(TURN_LEFT);
				}
				else {
					rotateVehicle(TURN_RIGHT);
				}
			}

		mAngle = (3.1415926536f / 180)*(mModel.getRotation());

		float forx = 0.2f*-sin(mAngle);
		float fory = 0.2f*cos(mAngle);

		moveVehicle(-forx*mMaxSpeed, -fory*mMaxSpeed);

	}

}